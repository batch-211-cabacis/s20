console.log("Hello World!")

// Repetition Control Structure (Loops)
	// Loops are one of the most important feature that programming must have
	// It lets us execute code repeatedly in a pre-set number or maybe forever

	// Mini activity 1


	function greeting(){
		console.log("Hi, Batch 211!")
	}
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();

	// or we can just use loops

	let countNum = 10;
	while(countNum !==0){
		console.log("This is printed inside the loop: " + countNum);
		greeting()
		countNum--; //DON"T FORGET OTHERWISE IT WILL BREAK YOUR LAPTOP/MACHINE
	}

	// While loop
	/*
	A while loop takes in an expression/condition
	If the condition evaluates to true, the statements inside the code block will be executed
	*/
	/*
		Syntax
			while(expression/condition){
				statement/code block
				final expression ++/--(iteration)
			}

			--; decrement is by 1
			++; increment is by 1

			-expression /condition - this are the unit of code that is being evaluated in our loop
			Loop will run while the condition or expression is true
			-statement/code block - code/instuctions that will be executed several times
			-Final expression - indicates how to advance the loop
	*/

	let count = 5;
	// while the value of count is not equal to zero
	while(count !==0){
		// the current value of count is printed out
		console.log("While: " + count);
		// decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
		count --; //DON"T FORGET OTHERWISE IT WILL BREAK YOUR LAPTOP/MACHINE
	};

	// let num = 20;
	// while(num !== 0){
	// 	console.log("While: num " + num);
	// 	num --; //DON"T FORGET OTHERWISE IT WILL BREAK YOUR LAPTOP/MACHINE
	// }

	// let digit = 5;
	// while(digit !==20){
	// 	console.log("While: digit " + digit)
	// 	digit ++; //DON"T FORGET OTHERWISE IT WILL BREAK YOUR LAPTOP/MACHINE
	// } 

	// let num1 = 1;
	// while(num1 === 2){
	// 	console.log("While: num1 " + num1)
	// 	num --; //DON"T FORGET OTHERWISE IT WILL BREAK YOUR LAPTOP/MACHINE
				//will not run because it did not meet the condition
	// }

	// Do while Loop
	/*
	 - A do-while loop works a lot like the while loop
	 But unlike while loops, do-while guarantee that the code will be executed at least once
	*/
	/*
		Syntax
		do{
			statement/code block
			finalExpression ++/--
		}while (expression/condition)
	*/

	/*
	How the do-while works:
	1. The statements in the "do block executes once"
	2. The message "Do while: " + number will be printed out in the console
	3. After executing once, the while statement will evaluate wehether to run the next iteration of the loop based on the given expression/condition
	4. If the expression/condition is not true, another iteration of the loop will be executed and will be repeated until the condition is met
	5. If the condition is true, the loop will stop
	*/

	// let number = Number(prompt("Give me a number"));
	// do{
	// 	console.log("Do while: " + number)
	// 	number += 1;
	// }while(number<10);

	// For Loop 
	// A For Loop is more flexible than while and do-while loops
	/*
		It consists of 3 parts:
		1. Initialization value that will track the progression of the loop
		2. Expression/condition that will be evaluated which will determine if the loop will run one more time
		3. Final expression indicates how to advance the loop
	*/

	/*
		Syntax
		 for(initialization; expression/condition; finalexpression){
			statement
		 }
	*/


	for(let count = 0; count <=20; count ++){
		console.log("For Loop: " + count);
	}

	// Mini activity 2

	for(let count = 0; count <=20; count ++){
		// console.log("For Loop: " + count);
		if(count % 2 == 0 && count !=0){
			console.log("Even: " + count);
		}
	};

	let myString = "Camille Doroteo";
	console.log(myString.length); // (.length)gives the number of all characters of the string  including spaces
	// strings are special compared to other data types

	// Accessing elements of a string
	// Individual characters of a string may be accessed using its index number
	// the first character in a string corresponds to the number 0, the next is 1...

	console.log(myString[2]);
	console.log(myString[0]);
	console.log(myString[8]);
	console.log(myString[14]);

	for(let x = 0; x < myString.length; x++){
		console.log(myString[x]);
	};

	// Mini activity 3

	let myFullName = "Angelique Cabacis"

	for(let x = 0; x < myFullName.length; x++){
		console.log(myFullName[x]);
	};

	let myName = "Nehemiah"
	let myNewName = "";
	for(let i=0; i<myName.length; i++){
		if(
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "u" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "o" 
			){
			// if the letter in the same name is vowel, it will print the number 3
			console.log(3);
		}else{
			// Print in the console all non-vowel characters in the name
			console.log(myName[i]);
			myNewName += myName[i]
			// += is addition assignment operator
		}
	};
	console.log(myNewName);
	// concatinate the characters based on the given condition


	// Continue and breaks statement
	/*
		The continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
		The break statement is used to terminate the current loop once a match has been found
	*/

	for(let count = 0; count <=20; count++){
		if(count % 2 === 0){
			console.log("Even Number")
			// this tells the console to continue to the next iteration of the loop
			continue;
			// ignores all the statements located after the contnue statement
			// console.log("Hello")
		}
		console.log('Continue and Break: ' + count);

		if(count>10){
			// tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
			// number values after will no longer be printed
			break;

		}
	}

	let name = "alexandro";
	for(let i = 0; i <name.length; i++){
		// the current letter is printed out based on its index
		console.log(name[i])
		if(name[i].toLowerCase()=== "a"){
			console.log("continue to the next iteration");
			continue;
		}
		if(name[i].toLowerCase() == "d"){
			break;
		}
	};
	
